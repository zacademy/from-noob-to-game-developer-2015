//This is your starting point

//Adding reference paths to the ts files used

/// <reference path="Lib/phaser.d.ts"/>
/// <reference path="States/Preloader.ts"/>


/* Rename "GameName" (this class and the file itself) to the name of the new game */
module GameName {
    class GameName extends Phaser.Game {

        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'phaser-div', {create: this.create});
        }


        create() {
            this.state.add("Preloader", Preloader, true);
        }
    }

    window.onload = () => {
        new GameName(1280,720);
    };
}