//This is your starting point
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
//Adding reference paths to the ts files used
/// <reference path="Lib/phaser.d.ts"/>
/// <reference path="States/Preloader.ts"/>
/* Rename "GameName" (this class and the file itself) to the name of the new game */
var GameName;
(function (_GameName) {
    var GameName = (function (_super) {
        __extends(GameName, _super);
        function GameName(width, height) {
            _super.call(this, width, height, Phaser.AUTO, 'phaser-div', { create: this.create });
        }
        GameName.prototype.create = function () {
            this.state.add("Preloader", _GameName.Preloader, true);
        };
        return GameName;
    })(Phaser.Game);
    window.onload = function () {
        new GameName(1280, 720);
    };
})(GameName || (GameName = {}));
//# sourceMappingURL=GameName.js.map