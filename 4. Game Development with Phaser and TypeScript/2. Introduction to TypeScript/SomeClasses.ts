class Animal{
    private name: string;

    constructor(theName: string){
        this.name = theName;
    }
    move(meters: number = 0){
        console.log(this.name + " moved " + meters + " m.")
    }
}

class Bunny extends Animal{
    constructor(bunnyName: string){
        super(bunnyName)
    }
    //move(meters= 6){
    //    super.move(meters)
    //}
}

class Horse extends Animal{
    constructor(horseName: string){
        super(horseName)
    }
    move(meters = 40){
        super.move(meters);
    }
}

var pesho = new Bunny("Pesho");
var gosho = new Horse("Gosho");

pesho.move();
gosho.move(30);

class GetBunny{
    private bunnyName: string;

    get fullName(): string{
        return this.bunnyName
    }

    set fullName(newName: string){
        if(newName.length <= 3){
            throw new Error("Unauthorized update of Bunny");
        }
        else{
            this.bunnyName = newName;
        }
    }
}
var bunny = new GetBunny();
bunny.fullName = "Pesho";

if(bunny.fullName){
    console.log(bunny.fullName);
}
