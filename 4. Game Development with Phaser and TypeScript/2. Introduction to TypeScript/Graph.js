///<reference path="Vertex.ts"/>
var SimpleGraph;
(function (SimpleGraph) {
    var Graph = (function () {
        function Graph(givenSetOfVertices) {
            this.vertices = givenSetOfVertices || [];
        }
        Graph.prototype.addVertexNow = function (value) {
            var vertex = new Vertex(value);
            this.vertices.forEach(function (v) {
                if (v.value === vertex.value) {
                    throw new Error("You are not allowed to have vertices with same values");
                }
            });
            this.vertices.push(vertex);
        };
        Graph.prototype.addDirectedEdge = function (from, to) {
            var vFrom = this.vertices[this.findByValue(from)];
            var vTo = this.vertices[this.findByValue(to)];
            vFrom.neighbours.push(vTo);
        };
        Graph.prototype.addUndirectedEdge = function (from, to) {
            this.addDirectedEdge(from, to);
            this.addDirectedEdge(to, from);
        };
        Graph.prototype.findByValue = function (value) {
            for (var i = 0; i < this.vertices.length; i++) {
                if (this.vertices[i].value === value) {
                    return i;
                }
            }
            return null;
        };
        return Graph;
    })();
    SimpleGraph.Graph = Graph;
})(SimpleGraph || (SimpleGraph = {}));
//# sourceMappingURL=Graph.js.map