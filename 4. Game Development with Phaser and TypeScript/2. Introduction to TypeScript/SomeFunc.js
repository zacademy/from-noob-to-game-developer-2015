function someFunction(x, y) {
    return x + y;
}
//console.log(someFunction(2, 3));
//
var addFunc = function (firstName, secondName) {
    return firstName + " " + secondName;
};
//console.log(addFunc("Pesho", "Petrov"));
function sayHI(name) {
    //console.log(("Hello" + name));
}
sayHI("Vasko");
function bunny(name, age, isAlive, familyName) {
    var newAge = age || 0;
    var isItAlive = isAlive || false;
    return name + " " + newAge + " " + " " + isItAlive;
}
var firstBunny = bunny("Gosho", 18, true);
var secondBunny = bunny("Pesho");
var thirdBunny = bunny("Tosho", 10, true, "Todorov");
var lambdaFunction = function (name) {
    return name;
};
//console.log(lambdaFunction("marto"));
//console.log(firstBunny);
//console.log(secondBunny);
//console.log(thirdBunny);
//# sourceMappingURL=SomeFunc.js.map