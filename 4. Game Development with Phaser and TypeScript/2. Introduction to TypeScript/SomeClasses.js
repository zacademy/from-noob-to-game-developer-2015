var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Animal = (function () {
    function Animal(theName) {
        this.name = theName;
    }
    Animal.prototype.move = function (meters) {
        if (meters === void 0) { meters = 0; }
        console.log(this.name + " moved " + meters + " m.");
    };
    return Animal;
})();
var Bunny = (function (_super) {
    __extends(Bunny, _super);
    function Bunny(bunnyName) {
        _super.call(this, bunnyName);
    }
    return Bunny;
})(Animal);
var Horse = (function (_super) {
    __extends(Horse, _super);
    function Horse(horseName) {
        _super.call(this, horseName);
    }
    Horse.prototype.move = function (meters) {
        if (meters === void 0) { meters = 40; }
        _super.prototype.move.call(this, meters);
    };
    return Horse;
})(Animal);
var pesho = new Bunny("Pesho");
var gosho = new Horse("Gosho");
pesho.move();
gosho.move(30);
var GetBunny = (function () {
    function GetBunny() {
    }
    Object.defineProperty(GetBunny.prototype, "fullName", {
        get: function () {
            return this.bunnyName;
        },
        set: function (newName) {
            if (newName.length <= 3) {
                throw new Error("Unauthorized update of Bunny");
            }
            else {
                this.bunnyName = newName;
            }
        },
        enumerable: true,
        configurable: true
    });
    return GetBunny;
})();
var bunny = new GetBunny();
bunny.fullName = "Pesho";
if (bunny.fullName) {
    console.log(bunny.fullName);
}
//# sourceMappingURL=SomeClasses.js.map