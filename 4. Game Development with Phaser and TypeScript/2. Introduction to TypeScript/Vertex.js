var Vertex = (function () {
    function Vertex(value, neighbours, colour) {
        this.value = value;
        this.neighbours = neighbours || [];
        this.colour = colour;
    }
    return Vertex;
})();
//# sourceMappingURL=Vertex.js.map