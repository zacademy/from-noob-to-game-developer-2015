function someFunction(x: number, y:number): number{
    return x+y;
}

//console.log(someFunction(2, 3));
//
var addFunc = function(firstName: string, secondName: string){
    return firstName + " " + secondName;
}

//console.log(addFunc("Pesho", "Petrov"));

function sayHI(name: string): void{
    //console.log(("Hello" + name));
}
sayHI("Vasko");

function bunny(name: string, age?: number, isAlive?: boolean, familyName?: string){
    var newAge = age ||0;
    var isItAlive = isAlive ||false;
    return name + " " + newAge + " " + " " + isItAlive
}

var firstBunny = bunny("Gosho", 18, true);
var secondBunny = bunny("Pesho");
var thirdBunny = bunny("Tosho", 10, true, "Todorov");

var lambdaFunction = (name: string) => {return name};

//console.log(lambdaFunction("marto"));
//console.log(firstBunny);
//console.log(secondBunny);
//console.log(thirdBunny);


