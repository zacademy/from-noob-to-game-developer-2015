
class Vertex<T>{
    value: T;
    neighbours: Vertex<T>[];
    colour: number;

    constructor(value:T, neighbours?: Vertex<T>[], colour?: number){
        this.value = value;
        this.neighbours = neighbours||[];
        this.colour = colour;
    }
}