﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._8_BitNumbers
{
    class EightBitNumbers
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());
            int[] arr = new int[num];
            
            //Recursive call
            Generate0or1(0, arr);
        }

        private static void Generate0or1(int index, int[] arr)
        {
            //Bottom
            if (index == arr.Length)
            {
                Console.WriteLine(string.Join(" ",arr));
                return;
            }
            ////Recursive call with 0
            //arr[index] = 0;
            //Generate0or1(index + 1, arr);

            ////Recursive call with 1
            //arr[index] = 1;
            //Generate0or1(index + 1, arr);

            //The code below is used to generate all numbers with 3(or more) digits base 4. 
            //The example above is for base 2.
            for (int i = 1; i <= 3; i++)
            {
                arr[index] = i;
                Generate0or1(index + 1, arr);
            }
        }
    }
}
