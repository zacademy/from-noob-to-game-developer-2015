﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Labyrinth
{
    class LabyrinthDemo
    {
        static void Main(string[] args)
        {
            char[,] matrix =
            {
                { ' ','*',' ','*',' ',' ',' '},
                { ' ','*',' ','*',' ','*',' '},
                { ' ',' ',' ',' ',' ',' ',' '},
                { ' ','*','*','*','*','*',' '},
                { ' ','*',' ',' ',' ','*',' '},
                { ' ',' ',' ','*',' ',' ','e'},
            };

            var lab = new Labyrinth(matrix);

            lab.FindExit(0,0);
        }
    }

    public class Labyrinth
    {
        private char[,] matrix;

        public Labyrinth(char[,] matrix)
        {
            this.matrix = matrix;
        }

        public void FindExit(int row, int col)
        {
            //bottom
            if (row<0 || row>=matrix.GetLength(0)||
                col<0 || col>=matrix.GetLength(1))
            {
                return;
            }
            //bottom
            if (matrix[row, col] == '*' || matrix[row,col] =='v')
            {
                //show with print matrix - this will be implemented in the lecture
                return;
            }
            //bottom
            if (matrix[row, col] == 'e')
            {
                PrintResult();
                return;
            }
            MarkCurrent(row, col);

            FindExit(row - 1, col); //up
            FindExit(row, col + 1); //right
            FindExit(row + 1, col); //down
            FindExit(row, col - 1); //left

            UnmarkCurrent(row, col);
            //Implement Solution via directions - this will be shown in the lecture
        }

        private void PrintResult()
        {
            for (int i = 0; i < this.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < this.matrix.GetLength(1); j++)
                {
                    Console.Write("{0}|", matrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("--------------");
            Console.WriteLine();
        }

        private void UnmarkCurrent(int row, int col)
        {
            this.matrix[row, col] = ' ';
        }

        private void MarkCurrent(int row, int col)
        {
            this.matrix[row, col] = 'v';
        }

    }
}
