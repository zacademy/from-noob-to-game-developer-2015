﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Combinations
{
    class CombinationsDemo
    {
        static void Main(string[] args)
        {
            Console.Write("start=");
            int start = int.Parse(Console.ReadLine());
            Console.Write("finish=");
            int finish = int.Parse(Console.ReadLine());
            Console.Write("k=");
            int num = int.Parse(Console.ReadLine());
            int[] arr = new int[num];

            Combinations(start, finish, 0, arr);

        }

        private static void Combinations(int start, int finish, int index, int[] arr)
        {
            if (index == arr.Length)
            {
                Console.WriteLine(string.Join(" ", arr));
                return;
            }
            for (int i = start; i <= finish; i++)
            {
                arr[index] = i;
                Combinations(i + 1, finish, index + 1, arr);
            }
        }
    }
}
