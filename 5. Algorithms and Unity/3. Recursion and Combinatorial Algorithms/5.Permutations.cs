﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permutations
{
    class PermutationsDemo
    {
        static void Main(string[] args)
        {
            Console.Write("n=");
            int num = int.Parse(Console.ReadLine());
            int[] arr = new int[num];
            bool[] used = new bool[num];          

            Permutations(0, arr, used);

        }

        private static void Permutations(int index, int[] arr, bool[] used)
        {
            if (index == arr.Length)
            {
                Console.WriteLine(string.Join(" ", arr));
                return;
            }
            for (int i = 0; i < arr.Length; i++)
            {
                if (!used[i])
                {
                    used[i] = true;
                    arr[index] = i;
                    Permutations(index + 1, arr,used);
                    used[i] = false;
                }
            }
        }
    }
}
