﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Fibonacci
{
    class FibonacciDemo
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 10; i++)
            {
                //Calling the recursion and printing the result on the console
                Console.WriteLine(Fibonacci(i)); 
            }
        }

        private static int Fibonacci(int num)
        {
            //Bottom
            if (num == 1)
            {
                return 1;
            }
            //Bottom
            if (num == 2)
            {
                return 1;
            }

            //Recursive call
            return Fibonacci(num - 1) + Fibonacci(num - 2);
        }
    }
}
