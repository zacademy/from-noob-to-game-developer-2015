﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Factorial
{
    class FactorialDemo
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());
            //Calling the recursion and printing the result on the console
            Console.WriteLine(Factorial(num));
        }

        private static int Factorial(int num)
        {
            //The bottom of the recursion
            if (num == 0)
            {
                return 1;
            }
            //The recursive call
            return num * Factorial(num - 1);
        }
    }
}
