﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trees
{
    public class Vertex<T>
    {
        private T value;
        private List<Vertex<T>> children;
        private bool hasParent;

        public Vertex(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Cannot create a vertex with null value!");
            }
            this.Value = value;
            this.children = new List<Vertex<T>>();
        }

        public T Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public int ChildrenCount
        {
            get { return this.children.Count; }
        }

        public void AddChild(Vertex<T> child)
        {
            if (child == null)
            {
                throw new ArgumentNullException("Cannot add child with null value!");
            }

            if (child.hasParent)
            {
                throw new ArgumentException("The vertex already has a parent!");
            }

            child.hasParent = true;
            this.children.Add(child);
        }

        public Vertex<T> GetChild(int index)
        {
            return this.children[index];
        }
    }
}
