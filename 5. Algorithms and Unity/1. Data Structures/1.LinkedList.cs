﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LinkedListDemo
{
    public class LinkedListNode<T>
    {
        public T Value { get; set; }

        public LinkedListNode<T> Previous { get; set; }

        public LinkedListNode<T> Next { get; set; }
    }

    public class LinkedList<T>:System.Collections.Generic.IEnumerable<T>
    {
        public LinkedListNode<T> First { get; set; }
        public LinkedListNode<T> Last { get; set; }

        public void AddLast(T value)
        {
            var newNode = new LinkedListNode<T>()
            {
                Value = value
            };

            if (this.First == null)
            {
                this.First = newNode;
                this.Last = newNode;
            }
            else
            {
                this.Last.Next = newNode;
                newNode.Previous = this.Last;
                this.Last = newNode;
            }
        }

        //Go To Testing
        //Then Implement enumerator Here
        public IEnumerator<T> GetEnumerator()
        {
            var node = this.First;

            while (node != null)
            {
                yield return node.Value;
                node = node.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        
    }
}
