﻿using System;


namespace LinkedListDemo
{
    class Testing
    {
        static void Main(string[] args)
        {
            int count = 10;
            var list = new LinkedList<int>();

            for (int i = 0; i < count; i++)
            {
                list.AddLast(i);
            }

            var node = list.First;
            while (node != null)
            {
                Console.WriteLine(node.Value);
                node = node.Next;
            }

            //After implementing iterator
            Console.WriteLine("-------------");
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
        }
    }
}
