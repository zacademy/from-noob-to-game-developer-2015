﻿using System;

namespace Trees
{
    public class Tree<T>
    {
        private Vertex<T> root;

        public Tree(T value, params Tree<T>[] children) : this(value)
        {
            foreach (Tree<T> child in children)
            {
                this.root.AddChild(child.root);
            }
        }

        public Tree(Vertex<T> root)
        {
            this.root = root;
        }

        public Tree(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Cannot create a tree with Null Root");
            }

            this.Root = new Vertex<T>(value);
        }

        public Vertex<T> Root
        {
            get { return this.root; }
            private set { this.root = value; }
        }
    }
}
