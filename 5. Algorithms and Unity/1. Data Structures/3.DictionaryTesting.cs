﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HashTables
{
    public class Bunny
    {
        public Bunny(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public override int GetHashCode()
        {
            return this.FirstName.GetHashCode() ^ this.LastName.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Bunny objectBunny = obj as Bunny;
            //What is the difference between the above line 
            // and (Bunny)obj?
            if (objectBunny == null)
            {
                return false;
            }
            return this.FirstName == objectBunny.FirstName && this.LastName == objectBunny.LastName;
        }
    }
    class Testing
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> myDictionary = new Dictionary<int, string>();
            myDictionary.Add(0, "bunny1");
            myDictionary[1] = "bunny2";
            var bunnyHash = "Pesho".GetHashCode();
            Console.WriteLine(bunnyHash);
            var date = new DateTime(1999, 7, 25);
            var dateHash = date.GetHashCode();

            var bunnyHashInTable = Math.Abs(bunnyHash % 10);

            Bunny myBunny = new Bunny("Pesho", "Peshov");
            //HashCodeStuff
            Bunny myOtherBunny = new Bunny("Pesho", "Peshov1");
            //HashCodeStuff     
            Console.WriteLine(myBunny.GetHashCode());
            Console.WriteLine(myOtherBunny.GetHashCode());
            //Check myBunny1.Equals(myBunny2)
            //This is important for collision purposes
        }
    }
}
