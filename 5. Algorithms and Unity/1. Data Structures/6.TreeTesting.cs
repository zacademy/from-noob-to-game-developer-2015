﻿using System;

namespace Trees
{
    class Testing
    {
        static void Main(string[] args)
        {
            Tree<int> tree =
        new Tree<int>(1,
                      new Tree<int>(2,
                                    new Tree<int>(5),
                                    new Tree<int>(6)),
                                  
                      new Tree<int>(3),
                      new Tree<int>(4,
                                    new Tree<int>(7),
                                    new Tree<int>(8),
                                    new Tree<int>(9)));
        }
    }
}
