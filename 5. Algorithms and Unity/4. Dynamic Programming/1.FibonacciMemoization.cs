﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.FibonacciMemo
{
    class Program
    {
        static int[] memo = new int[10];
        static void Main(string[] args)
        {
            Console.WriteLine(Fibonacci(5));
        }

        private static int Fibonacci(int n)
        {
            if (memo[n] != 0)
            {
                return memo[n];
            }
            if (n == 0 || n == 1)
            {
                return 1;
            }

            memo[n] = Fibonacci(n - 1) + Fibonacci(n - 2);
            return memo[n];
        }
    }
}
