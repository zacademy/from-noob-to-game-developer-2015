﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Given a set, is there a subset which sums to 0, or to S
namespace _2.SubsetSum
{
    class SubsetSum
    {
        static void Main(string[] args)
        {
            isThereSum(new int[] { 1, -1, 3 }, 0);
        }

        private static bool isThereSum(int[] elements, int sum)
        {
           
            var possibleSumsDict = new Dictionary<int, bool>();
            possibleSumsDict[0] = true;

            var possibleSums = new List<int>();
            possibleSums.Add(0);

            for (int i = 0; i < elements.Length; i++)
            {
                var possibleSumsToAdd = new List<int>();

                foreach (var pSum in possibleSums)
                {
                    possibleSumsToAdd.Add(pSum + elements[i]);
                }

                for (int j = 0; j < possibleSumsToAdd.Count; j++)
                {
                    if (!possibleSumsDict.ContainsKey(possibleSumsToAdd[j]) 
                        || !possibleSumsDict[possibleSumsToAdd[j]])
                    {
                        possibleSumsDict[possibleSumsToAdd[j]] = true;
                        possibleSums.Add(possibleSumsToAdd[j]);
                    }
                }
            }

            for (int i = 0; i < possibleSums.Count; i++)
            {
                Console.WriteLine(possibleSums[i]);
            }

            return false;
        }
    }
}
