﻿using System;

//Create and count all possible words out of 4 letters, so that no word has 2 identical letters next to each other.
//Each letter can be used a specified number of times.

namespace _1.Wordz
{
    class Wordz
    {
        static int[,,,,] memo = new int[20, 20, 20, 20, 5];

        static void Main(string[] args)
        {
            
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());
            int d = int.Parse(Console.ReadLine());

            Console.WriteLine(PlaceLetters(a, b, c, d, 4));
        }

        private static int PlaceLetters(int a, int b, int c, int d, int lastLetter)
        {

            if (a + b + c + d == 0)
            {
                return 1;
            }

            if (memo[a, b, c, d,lastLetter] > 0)
            {
                return memo[a, b, c, d, lastLetter];
            }

            int count = 0;

            if (a>0 && lastLetter!=0)
            {
                
                count+=PlaceLetters(a - 1, b, c, d, 0); 
            }
            if (b>0 && lastLetter!=1)
            {
                
                count+=PlaceLetters(a, b - 1, c, d, 1); 
            }
            if (c>0 && lastLetter!=2)
            {
                
                count+=PlaceLetters(a, b, c - 1, d, 2); 
            }
            if (d>0 && lastLetter!=3)
            {
                count+=PlaceLetters(a, b, c, d - 1, 3); 
            }

            memo[a, b, c, d, lastLetter] = count;

            return count;
        }
    }
}
