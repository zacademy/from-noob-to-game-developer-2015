﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LargestIncreasingSubsequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sampleArr = { 1, 8, 2, 7, 3, 4, 1, 6 };
            largestIncreasingSubseqence(sampleArr);
        }

        private static int[] largestIncreasingSubseqence(int[] arr)
        {
            int[] maximalSubsequenceCount = new int[arr.Length];
            maximalSubsequenceCount[0] = 1;
            for (int i = 1; i < arr.Length; i++)
            {
                var maxSeqHelper = new List<int>();
                for (int j = 0; j < i; j++)
                {
                    
                    if (arr[i] > arr[j])
                    {
                        maxSeqHelper.Add(maximalSubsequenceCount[j] + 1);
                    }
                }

                if (maxSeqHelper.Count > 0)
                {
                    maximalSubsequenceCount[i] = maxSeqHelper.Max(); 
                }
            }
            Console.WriteLine(maximalSubsequenceCount.Max());
            return new int[5];
        }
    }
}
