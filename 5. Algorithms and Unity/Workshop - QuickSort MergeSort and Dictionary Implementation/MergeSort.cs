﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mergesorting
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] testArr = { 5, 6, 2, 3, 1, 7, 2};
            testArr = MSort(testArr);
            Console.WriteLine(string.Join(", ", testArr));
        }

        private static int[] MSort(int[] arr)
        {
            if (arr.Length <= 1)
            {
                return arr;
            }

            int midPoint = arr.Length / 2;

            int[] left = new int[midPoint];
            int[] right = new int[(arr.Length+1)/2];

            int[] result = new int[arr.Length];

            for (int i = 0; i < midPoint; i++)
            {
                left[i] = arr[i];
            }

            int j = 0;

            for (int i = midPoint; i < arr.Length; i++)
            {
                    right[j] = arr[i];
                    j++;
            }

            left = MSort(left);
            right = MSort(right);

            result = Merge(left,right);
            return result;
        }

        private static int[] Merge(int[] left, int[] right)
        {
            int resultLength = left.Length + right.Length;
            int[] result = new int[resultLength];
            int leftIndex = 0;
            int rightIndex = 0;
            int resultIndex = 0;

            while (leftIndex<left.Length || rightIndex<right.Length)
            {
                if (leftIndex < left.Length && rightIndex < right.Length)
                {
                    if (left[leftIndex] <= right[rightIndex])
                    {
                        result[resultIndex] = left[leftIndex];
                        leftIndex++;
                        resultIndex++;
                    }
                    else
                    {
                        result[resultIndex] = right[rightIndex];
                        rightIndex++;
                        resultIndex++;
                    }
                }

                else if (leftIndex < left.Length)
                {
                    result[resultIndex] = left[leftIndex];
                    leftIndex++;
                    resultIndex++;
                }

                else if (rightIndex < right.Length)
                {
                    result[resultIndex] = right[rightIndex];
                    rightIndex++;
                    resultIndex++;
                }
            }
            return result;
        }
    }
}
