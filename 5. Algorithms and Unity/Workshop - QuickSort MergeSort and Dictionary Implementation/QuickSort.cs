﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] testArr = { 5, 6, 2, 3, 1, 7, 2 };
            qSort(testArr);
            Console.WriteLine(string.Join(", ",testArr));
        }

        private static void qSort(int[] arr)
        {
            if (arr.Length <= 1)
            {
                return;
            }

            Sort(arr, 0,arr.Length-1);
        }

        private static void Sort(int[] arr, int start, int end)
        {
            if ((end - start) <= 0)
            {
                return;
            }

            int splitPoint = Split(arr, start, end);
            Sort(arr, start, splitPoint - 1);
            Sort(arr, splitPoint, end);
        }

        private static int Split(int[] arr, int start, int end)
        {
            int left = start + 1;
            int right = end;
            int pivot = arr[start];

            while (true)
            {
                //Moving Left
                while (left <= right && arr[left] < pivot)
                    left++;

                //Moving Right
                while (right > left && arr[right] > pivot)
                    right--;

                if (left >= right)
                    break;

                //swap items
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
                start++;
                end--;
            }

            arr[start] = arr[left - 1];
            arr[left - 1] = pivot;
            return left;
        }
    }
}
