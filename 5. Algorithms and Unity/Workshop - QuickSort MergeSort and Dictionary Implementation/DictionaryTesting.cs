﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryDemo
{
    class Testing
    {
        static void Main(string[] args)
        {
            var myDict = new Dict<string,int> ();

            myDict.Add("Bunny1", 3);
            myDict.Add("Bunny2", 2);
            myDict["Bunn3"] = 5;
            myDict["Bunny1"] = 5;
            foreach (var bunny in myDict)
            {
                Console.WriteLine("key: {0}, val: {1}",bunny.Key,bunny.Value);
            }

            
            Console.WriteLine(myDict.ContainsKey("Bunny1"));
            Console.WriteLine(myDict.ContainsKey("Bunny3"));

            Console.WriteLine(myDict.Find("Bunny1"));


        }
    }
}
