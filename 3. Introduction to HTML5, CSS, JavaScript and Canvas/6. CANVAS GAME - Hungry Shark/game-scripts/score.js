function Score() {
    this.score = 0;
}

Score.prototype.draw =  function() {
    context.fillStyle = "black";
    context.font = "bolder 20px Papyrus";
    context.fillText("Score: " + ((this.score / 100) | 0).toString(), 660, 20);
}

Score.prototype.update = function() {
    this.score += 10;

    if (healthBar.isFishEaten) {
        this.score += 5000;
    }

    healthBar.isFishEaten = false;
};