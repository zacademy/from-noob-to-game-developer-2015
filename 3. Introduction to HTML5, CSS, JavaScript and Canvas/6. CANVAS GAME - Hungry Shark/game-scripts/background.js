function BackgroundFeature(context, imageSource, positionY, velocityX) {
    this.image = new Image();
    this.image.src = imageSource;
    this.x = 0;
    this.y = positionY;
    this.velocityX=velocityX;
}

BackgroundFeature.prototype.draw = function () {

    context.drawImage(this.image, this.x, this.y);
    context.drawImage(this.image, this.image.width - Math.abs(this.x), this.y);

    if (Math.abs(this.x) > this.image.width) {
        this.x = 0;
    }

    this.x -= this.velocityX;
};
