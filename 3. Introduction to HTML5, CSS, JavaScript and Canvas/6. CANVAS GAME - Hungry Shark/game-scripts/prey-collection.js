function PreyCollection(){
    this.preyArray = [];
    this.generatePreyFrequency = 0;
}

PreyCollection.prototype.update = function(){
    this.generatePreyFrequency += 5;
    if (this.generatePreyFrequency === 100) {
        this.preyArray.push(new Prey());
        this.generatePreyFrequency = 0;
    }

    for (var i = 0, len = this.preyArray.length; i < len; i += 1) {
        var currentPrey = this.preyArray[i];
        currentPrey.update();
        if ((currentPrey.x < 0) || (currentPrey.isEaten())) {
            this.preyArray.splice(i, 1);
            i -= 1;
            len -= 1;
        }
    }
}

PreyCollection.prototype.draw = function () {
    for (var i = 0, len = this.preyArray.length; i < len; i += 1) {
        this.preyArray[i].draw();
    }
};