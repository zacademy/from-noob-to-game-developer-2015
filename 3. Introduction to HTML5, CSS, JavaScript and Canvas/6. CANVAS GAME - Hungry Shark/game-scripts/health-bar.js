function HealthBar(maxhp, constantDecreaseHP, fishIncrementHP) {
    this.maxHP = maxhp;
    this.currentHP = this.maxHP;
    this.percentageHP = 1;
    this.constantDecreaseHP = constantDecreaseHP;
    this.fishIncrementHP = fishIncrementHP;

    this.constantHPRectangleWidth = 334;
    this.isFishEaten = false;
}

HealthBar.prototype.draw = function () {
    var greyGradient = context.createLinearGradient(0,0,200,0);
    greyGradient.addColorStop(0,"#dadada");
    greyGradient.addColorStop(1,"#f3f3f3");
    context.fillStyle=greyGradient;
    context.lineWidth=0.5;
    context.strokeStyle="#7e7e7e";
    context.fillRect(9,9,334,23);
    context.strokeRect(9,9,334,23);

    var redGradient = context.createLinearGradient(0,0,200,0);
    redGradient.addColorStop(0,"#c14232");
    redGradient.addColorStop(1,"#f6928b");
    context.fillStyle = redGradient;
    context.fillRect(13,13,this.constantHPRectangleWidth*0.98*this.percentageHP,15);
    context.strokeRect(13,13,this.constantHPRectangleWidth*0.98*this.percentageHP,15);
};

HealthBar.prototype.update = function () {

    this.currentHP -= this.constantDecreaseHP;

    if (this.currentHP <= 0) {
        this.constantHPRectangleWidth = 0;
    }

    if (this.isFishEaten) {
        this.currentHP += this.fishIncrementHP;
    }

    if (this.currentHP > this.maxHP) {
        this.currentHP = this.maxHP;
    }

    this.percentageHP = this.currentHP / this.maxHP;
};
