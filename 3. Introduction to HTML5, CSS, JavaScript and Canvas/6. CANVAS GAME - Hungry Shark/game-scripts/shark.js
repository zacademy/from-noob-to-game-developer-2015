/**
 * Created by Fractal Games on 8/3/2015.
 */
function Shark(context, initialX, initialY) {
    this.x = initialX;
    this.y = initialY;
    this.velocityY = 0;
    this.GRAVITY = 0.2;
    this.JUMP_HEIGHT = 7;

    // Create sprite
    this.sharkImage = new Image();
    this.sharkImage.src = "images/shark.png";

    this.sharkSpr = sprite({
        context: canvas.getContext("2d"),
        width: 840,
        height: 100,
        x: this.x,
        y: this.y,
        image: this.sharkImage,
        numberOfFrames: 3,
        ticksPerFrame: 8
    });
}

Shark.prototype.draw = function(){
    this.sharkSpr.update();
    this.sharkSpr.render(this.x, this.y);
};

Shark.prototype.update = function () {
    this.y += this.velocityY;
    this.velocityY += this.GRAVITY;
};
Shark.prototype.jump = function () {
    this.velocityY = 0.5;
    this.velocityY -= this.JUMP_HEIGHT;
};