function Prey() {

    this.x = 750;
    this.y = Math.floor(Math.random() * 520 + 60);
    this.VELOCITY_X = Math.random()*2+5;

    var fishImage = new Image();
    fishImage.src = "images/prey-fish.png";

    this.fishSpr = sprite({
        context: canvas.getContext("2d"),
        width: 130,
        height: 40,
        x: this.x,
        y: this.y,
        image: fishImage,
        numberOfFrames: 2,
        ticksPerFrame: 10
    });
}

Prey.prototype.isEaten = function () {

    if ((this.x > 300 && this.x < 340) && (shark.y <= this.y) && (shark.y + 60 >= this.y)) {
        biteSound.play()
        var xBlood = this.x;
        var yBlood = this.y;

        context.drawImage(bloodImage, xBlood - 50, yBlood - 50);
        healthBar.isFishEaten = true;
        return true;
    } else {
        return false;
    }
}
Prey.prototype.update = function () {
    this.x -= this.VELOCITY_X;

};

Prey.prototype.draw = function() {
    this.fishSpr.update();
    this.fishSpr.render(this.x, this.y);
}