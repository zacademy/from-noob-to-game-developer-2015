var PetShop = (function(){ //namespace in c#, it is called a module in javaScript
    var Animal= (function(){//abstract/base class
        function Animal(name, age){//constructor
            this.name = name;//property
            this.age = age;//property
        }
        Animal.prototype.introduce = function(){ //method
            return "Hi! My name is " + this.name +
                " and I am " + this.age + " years old."
        };
        Animal.prototype.jump = function (){ //method
            return "I jumped";
        }
        return Animal;
    }());

    var Bunny = (function (){
        function Bunny(name, age, breed){
            Animal.call(this, name, age); //reusing constructor from Animal, like the base keyword
            this.breed=breed;

        }
        Bunny.prototype = new Animal(); //inheritance (rofl)
        Bunny.prototype.introduce = function(){
            return Animal.prototype.introduce.call(this)//reusing method from base class
                + ". I am a Bunny.";
        }
        return Bunny;
    }());

    var Cat = (function(){
        function Cat(name, age, furType){
            Animal.call(this,name,age);
            this.furType = furType;
        }
        Cat.prototype = new Animal();
        Cat.prototype.sleep = function(){
            return "Shhhh! I am sleeping..."
        }
        Cat.prototype.jump = function(){
            return Animal.prototype.jump.call(this)+ " higher than the bunny! Meow!"
        }
        return Cat;
    }());

    return {
        Animal:Animal,
        Bunny:Bunny,
        Cat:Cat,
    }
}());//IIFE

var bunny = new PetShop.Bunny("Pesho",12,"Cute White");
console.log(bunny.introduce());
var animal = new PetShop.Animal("Gosho",1);
console.log(animal.introduce());
var cat = new PetShop.Cat("Mark",30,"none");
console.log("-".repeat(20));
console.log(cat.name);
console.log(cat.age);
console.log(cat.furType);
console.log(cat.jump());
console.log(cat.introduce());
console.log(cat.sleep());