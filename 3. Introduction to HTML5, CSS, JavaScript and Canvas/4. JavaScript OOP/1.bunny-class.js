////Think of this function as a class
//function Bunny(name, age){
//    //these act like properties
//    this.name=name;
//    this.age=age;
//    }
//Bunny.prototype.sayHi = function() {
//    return "Hi! My name is " + this.name;
//}
//var bunny = new Bunny("Pesho",15);
//console.log(bunny.age);
//console.log(bunny.sayHi());
//console.log("---------------------------");
//var bunny1 = new Bunny();
//var bunny2 = new Bunny();
//console.log(bunny1.sayHi === bunny2.sayHi);