/**
 * Created by Zacademy on 7/23/2015.
 */
function printLogo(){
    console.log("Hello!");
}
printLogo();

var func1 = function printLogo2(){
    console.log("Hello, again!"
    );
}();
var func2 = function(){
    console.log("and again!");
}();

var func3 = new Function("console.log('hi')");

console.log("--------------");
function compareTwoNumbers(a,b){
    if(a>b){
        console.log(a + " is bigger");
        return a;
    }
    else{
        console.log(b + " is bigger");
        return b;
    }
}
var result = compareTwoNumbers(7,2);
console.log("--------------");
function imitatingOverloading(){
    switch(arguments.length) {
        case 1:
            console.log(arguments[0]);
            break;
        case 2:
            console.log("we have 2 variables now");
            break;
    }
}
