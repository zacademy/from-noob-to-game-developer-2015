/**
 * Created by Zacademy on 7/23/2015.
 */
var bunny={
 firstName:"Fuzzy",
    lastName:"Wuzzy",
    age:5,
    toStringForBunny:function(){
        return (bunny.firstName + " " + bunny.lastName + " " + bunny.age);
    }
}

console.log(bunny.toStringForBunny());
function makeBunny(firstName,lastName,age) {
    return {
        firstName: firstName,
        lastName: lastName,
        age: age,
        toStringForBunny: function () {
            return (this.firstName + " " + this.lastName + " " + this.age);
        }
    }
}
console.log(bunny.toStringForBunny());

//C# syntax this is similar to: Bunny bunny = new Bunny(pesho....)